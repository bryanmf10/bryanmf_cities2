import React, { useState, useEffect } from "react";
import { Button } from "reactstrap";
import styled from "styled-components";

const Cuerpo = styled.body`
  background-color: black;
`;
const Caja = styled.div`
    position: relative;
    display: inline-block;
`;

const Texto = styled.div`
    position: absolute;
    top: 80%;
    right: 10%;
    font-size: 16px;
    color: white;
`;


const Cites = () => {
  const [llista, setLlista] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [citarandom, setCitarandom] = useState(Math.floor(Math.random() * 10) + 1);

  useEffect(() => {
    const apiUrl = `http://localhost:3001/api/${citarandom}`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setLlista(data);
        setLoading(false);
      })
      .catch((error) => setError(true));

    console.log("llista");
    console.log(llista.autor);
    console.log("peticion enviada");

  }, [citarandom]);

  if (error) {
    return <h3>Se ha producido un error...</h3>;
  }


  if (loading) {
    return <h3>loading...</h3>;
  }

  console.log("llista");
  console.log(llista);

  const mostrar = (x) => {
    setCitarandom(x);
    const autor = llista.autor;
    const texto = llista.texto;
  };

  return (
    // <>
      <Cuerpo>

      <Button onClick={() => mostrar(Math.floor(Math.random() * 10) + 1)}>Cita Random</Button>
      <Caja>
      <img src={llista.foto}></img>
      <Texto>
             {llista.texto}
             <br/>
             <br/>
             <br/>
             {llista.autor}
      </Texto>
      </Caja>
      </Cuerpo>

    // </>
  );
};

export default Cites;
